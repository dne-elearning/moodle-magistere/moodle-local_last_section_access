<?php
 
function add_last_section_access($courseid, $userid, $sectionid){
    global $DB;
    if($courseid != null && $userid != null && $sectionid != null){
        if( $DB->record_exists('local_last_section_access',array('courseid' => $courseid , 'userid' => $userid))){
            $last_section_access = $DB->get_record('local_last_section_access',array('courseid' => $courseid , 'userid' => $userid));
            $last_section_access->sectionid = $sectionid;
            $last_section_access->timecreated = time();

            $DB->update_record('local_last_section_access', $last_section_access);
        }else{
            $last_section_access = new stdClass();
            $last_section_access->courseid = $courseid;
            $last_section_access->userid = $userid;
            $last_section_access->sectionid = $sectionid;
            $last_section_access->timecreated = time();

            $DB->insert_record('local_last_section_access', $last_section_access);
        }
    }
    return false;
}

function check_participant($userid,$courseid){
    global $DB;
    $sql = "SELECT r.shortname  FROM {user_enrolments} ue 
INNER JOIN {user} u ON u.id = :userid 
INNER JOIN {enrol} e ON (e.id = ue.enrolid)
INNER JOIN {course} c ON (c.id = e.courseid)
INNER JOIN {context} cx ON (contextlevel=50 AND c.id = cx.instanceid)
INNER JOIN {role_assignments} ra ON (cx.id = ra.contextid AND ra.userid = u.id)
INNER JOIN {role} r ON (r.id = ra.roleid)
WHERE ue.userid = u.id
AND c.id = :courseid";
    $param = array('userid' => $userid, 'courseid' => $courseid);
    $results = $DB->get_records_sql($sql,$param);

     if(count($results) > 0){
         if(isset($results['participant'])){
             return true;
         }
    }

    return false;
}

function is_session_categorie($courseid){
    global $DB;
    $course = $DB->get_record('course', array('id' => $courseid) );
    if($course){
        $coursecat = $DB->get_record('course_categories' , array('id'=>$course->category));
        if($coursecat){
            $sessioncat = $DB->get_record('course_categories' , array( 'name' => 'Session de formation' , 'depth' => 1), '*', IGNORE_MULTIPLE);
            if($sessioncat){
                if (strpos($coursecat->path, '/'.$sessioncat->id.'/') === 0){
                    return true;
                }
            }
        }
    }
    return false;
}

function get_redirection($userid, $courseid, $sessiondefault = 1){
    global $DB;
    $record = $DB->get_record('local_last_section_access',array('courseid' => $courseid , 'userid' => $userid));
    if($record){
        $section = $DB->get_record('course_sections', array('id' => $record->sectionid));
        if($section && $section->section != $sessiondefault){
            return $section->section;
        }
    }
    return null;
}

function get_redirection_link($userid, $courseid) {
    $redirectsection = get_redirection($userid, $courseid);
    if($redirectsection){
        $link = new moodle_url('/course/view.php', array('id' => $courseid, 'section' => $redirectsection));
        return $link;
    }
    return null;
}

function get_lastsectionaccess_button_title() {
    return get_string('resume', 'local_last_section_access');
}

function get_lastsectionaccess_button_desc() {
    return get_string('resume_description', 'local_last_section_access');
}