<?php

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Last section viewed';
$string['resume'] = 'Reprendre';
$string['resume_description'] = 'Resume from the last visited section';