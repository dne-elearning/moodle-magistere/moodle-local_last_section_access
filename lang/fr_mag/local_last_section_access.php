<?php

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Dernière section consultée';
$string['resume'] = 'Reprendre';
$string['resume_description'] = 'Reprendre cette formation là où j\'en étais lors de ma dernière visite';