<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in local myindex.
 *
 * @package    local_last_section_access
 * @copyright  2020 TCS
 * @author     TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/local/last_section_access/lib.php');

defined('MOODLE_INTERNAL') || die();


/**
 * Event observer for local_last_section_access.
 */
class local_last_section_access_observer {
    public static function course_viewed(core\event\course_viewed $event) {
        global $DB;

        // We keep compatibility with 2.7 and 2.8 other['coursesectionid'].
        $sectionid = null;
        if (!empty($event->other['coursesectionnumber'])) {
            $sectionid = $event->other['coursesectionnumber'];
        } else if (!empty($event->other['coursesectionid'])) {
            $sectionid = $event->other['coursesectionid'];
        }

        //check if user is just a participant
        if(check_participant($event->userid , $event->courseid)){
            //check if course is a "Session de formation" categorie
            if(is_session_categorie($event->courseid)){
                $section = $DB->get_record('course_sections', array('course'=>$event->courseid,'section' => $sectionid));
                if($section){
                    add_last_section_access($event->courseid, $event->userid, $section->id);
                }
            }
        }
    }
}