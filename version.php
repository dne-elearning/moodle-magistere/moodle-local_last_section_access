<?php

/**\r
 * @package   local_last_section_access\r
 * @author     TCS\r
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later\r
 */

$plugin->version = 2020121500;
$plugin->requires = 2020061502;
$plugin->cron = 0;
$plugin->component = 'local_last_section_access';
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '1.0';